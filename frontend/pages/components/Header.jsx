import React from "react";
import Image from "next/image";
import Link from "@mui/material/Link";
import logo from "../../public/icons/logo.png";
import burgerMenu from "../../public/icons/burger-menu.png";

const Header = () => {
  return (
    <div className="container" lg>
      <Image src={logo} alt="some" width={180} height={45} />
      <div className="nav-list">
        <Link href="">Services</Link>
        <Link href="">Business</Link>
        <Link href="">Clients</Link>
        <Link href="">Team</Link>
        <Link href="">Blog</Link>
      </div>
      <div className="nav-burger">
        <Image src={burgerMenu} alt="some" width={50} height={30} />
      </div>
    </div>
  );
};

export default Header;
